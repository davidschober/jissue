# Jissue() 

Jissue is small library for working with jstor metadata.xml files. The jissue class flattens metadata describing jstor issues into easily accessible dictionaries and lists. The goal of this project is to give users a quick and easy way to deal with standard jstor issue metadata, build up databases of content (pdfs and ocrs), develop content sites with data, or parse content metadata and data for analysis. 

I tried to keep the dictionary key names as close as possible to the jstor xml element and attribute names. 

NOTE: This is using Elementree. It's unforgiving. If the xml is broken, it won't parse.

## Install

Use pip to install directly from bitbucket. Alternately feel free to fork.

    :::bash
    $ pip install git+https://bitbucket.org/davidschober/jissue.git

## Quick start

    :::python

    #create a class
    >>> from jissue import Jissue
    >>> test_issue = Jissue()
    
    # return key value for issue metadata
    >>> for key, value in test_issue.metadata.items():
    ...     print "%s: %s" %(key, value)
    ... 
    issn: 00322032
    pubdate-datetime: 1915-06-01
    pagerange: 107-160
    journal-id: 10.2307/j50000596
    month: 6
    volume: 6
    string-date: Jun., 1915
    journal-title: Poetry
    year: 1915
    issue: 3
    day: 1

    #iterate through the titles of a couple of articles:
    >>> for article in test_issue.articles[:2]:
    ...     print "%s (doi: %s)" %(article['article-title'], article['identifier'])
    ... 
    The Syrian Lover in Exile Remembers Thee, Light of My Land (doi: 10.2307_20570413)
    Alma Mater (doi: 10.2307_20570414)

    #Parse a bunch of metdata and print out TOCs, you know becuase you like
    #to watch things roll by...
    >>> import os
    >>> years = [os.path.join(path_to_jstor, year) for year in os.listdir(path_to_jstor) if os.path.isdir(os.path.join(path_to_jstor, year))]
    >>> issue_years = [[os.path.join(year, issue) for issue in os.listdir(year) if os.path.isdir(os.path.join(year, issue))] for year in years]
    >>> for issues in issue_years[20:80]:                                                                                         
    ...     for issue in issues:
    ...         parsed_issue = Jissue()
    ...         parsed_issue.parse(issue)
    ...         parsed_issue.print_toc()

    # A lot of stuff scrolls by very fast.

## Jstor Issue Attributes and Key Methods 

Here's a list of attributes

### article\_pages
A dictionary using article doi (identifier) as a key.

    :::python
    >>> test_issue.article_pages
    {'10.2307_20570437': ['p-52', 'p-53'], 
        '10.2307_20570436': ['p-51', 'p-52'], 
        '10.2307_20570435': ['p-51'], 
        '10.2307_20570434': ['p-50'], ... ETC }

### articles

A list of dictionaries containing all article information. This is the critical one.

    :::python
    [{'scans': ['pages/1.tif', 'pages/2.tif', 'pages/3.tif'], #page scans
        'article-type': 'research-article', #type of article 
        'group': 'I Sing of My Life while I Live It', #TOC group for article
        'url': 'http://www.jstor.org/stable/20570413?origin=pubexport', #jstor url
        'article-title': 'The Syrian Lover in Exile Remembers Thee, Light of My Land', #title
        'lpage': '109', 'fpage': '107', #last page as printed in magazine
        'identifier': '10.2307_20570413', #DOI of article
        'contrib-group': [{'given-names': 'Ajan', 'surname': 'Syrian', 'string-name': None}], # list of authors
        'ocrs': ['ocrs/1.xml', 'ocrs/2.xml', 'ocrs/3.xml']}, #OCRs of articles
        {'scans': ['pages/4.tif', 'pages/5.tif'], #New article 
        'article-type': 'research-article', 
        'group': 'I Sing of My Life while I Live It', 
        'url': 'http://www.jstor.org/stable/20570414?origin=pubexport', 
        'article-title': 
        'Alma Mater', 
        'lpage': '111', 
        'fpage': '110', 
        'identifier': '10.2307_20570414', 
        'contrib-group': [{'given-names': 'Ajan', 'surname': 'Syrian', 'string-name': None}], 
        'ocrs': ['ocrs/4.xml', 'ocrs/5.xml']}, 
        ... ETC
        ]

### issue\_dict 
Empty for now

### issue\_xml
Elementree representation of the xml.

### metadata

Key issue metadata. Date, DOI of whole issue, etc.

    :::python
    >>> test_issue.metadata
    {'issn': '00322032', 
    'pubdate-datetime': datetime.date(1915, 6, 1), 
    'pagerange': '107-160', 
    'journal-id': '10.2307/j50000596', 
    'month': '6', 
    'volume': '6', 
    'string-date': 'Jun., 1915', 
    'journal-title': 'Poetry', 
    'year': '1915', 
    'issue': '3', 
    'day': '1'}

### ocrs

A flat list of ocrs

### page\_ids
A flat list of page ids. These can be zipped with ocrs or scans to create a key/value pair.
e.g.

    :::python
    >>> dict(zip(test_issue.page_ids, test_issue.ocrs))
    {'p-4': 'ocrs/4.xml', 'p-5': 'ocrs/5.xml', 'p-6': 'ocrs/6.xml', 'p-7': 'ocrs/7.xml', ... ETC}

### scans

A flat list of scanned pages. Can be zipped with ocrs or page ids. 

To get ocrs paired with scans:
    
    :::python
    >>> zip(test_issue.ocrs, test_issue.scans)
    [('ocrs/1.xml', 'pages/1.tif'), ('ocrs/2.xml', 'pages/2.tif'), ('ocrs/3.xml', 'pages/3.tif'), ... ETC]

### print_toc()

A method that allows you to print a quick and dirty TOC
    
    :::python
    >>> test_issue.print_toc()
    # TOC 

    ## Section: I Sing of My Life while I Live It 

    **The Syrian Lover in Exile Remembers Thee, Light of My Land** 
      * doi: 10.2307_20570413 
      * Ajan Syrian
    **Alma Mater** 
      * doi: 10.2307_20570414 
      * Ajan Syrian
    **After Vespers** 
      * doi: 10.2307_20570415 
      * Ajan Syrian
    **To Rupert Brooke** 
      * doi: 10.2307_20570416 
      * Arthur Davison Ficke

### get_article_by_doi(DOI)

Grab a single article dictionary from the article list by DOI.

    :::python
    >>> test_issue.get_article_by_doi('10.2307_20570413')
    {'scans': ['pages/1.tif', 'pages/2.tif', 'pages/3.tif'], 
    'article-type': 'research-article', 
    'group': 'I Sing of My Life while I Live It', 
    'url': 'http://www.jstor.org/stable/20570413?origin=pubexport', 
    'article-title': 'The Syrian Lover in Exile Remembers Thee, Light of My Land', 
    ... ETC }

### xml\_root
Elementree object of xml root.

