class Jissue(object):
    """ This grabs an issue and creates a dictionary with all critical information. 
    I flattened out the data as much as possible to make it easier to work with. 
    """


    def __init__(self):
        """Grab the issue metadata"""
        import json 
        # These are the issue attributes set to empty
        self.issue_dict = {} # the issue dict
        self.metadata = {} #issue metadata including page range, date, etc
        self.articles = [] # a list of articles with contributors and group
        self.toc = [] # full nested list of article ids in TOC
        self.article_pages = {} # a dictionary of article pages {'DOI': [p-id, p-id] ...}  
        self.page_ids = [] # a list of page ids
        self.scans = [] # a list of scan hrefs
        self.ocrs = [] # a list of ocr hrefs

        
    def get_items_by_name(self, child, items):
        """grabs elements relative to a child and returns a list. Handy for cherry picking elements. 
        It grabs only the text from an element. If you're looking for attributes, use something else.
        I might extend to allow for attributes as well. Not this only works for single items
        """
       
        item_list = []
        for item in items:
            #make sure we have one            
            if child.find(".//"+item) is not None:
                item_list.append(child.find(".//"+item).text)
            else:
                item_list.append(None)
        #return a dictionary 
        return dict(zip(items,item_list))

    def get_xml_root(self,issue_path):
        """Grabs important elements from within the journal and issue meta"""
        
        import os
        import xml.etree.ElementTree as et

        issue_file = os.path.join(issue_path, "metadata.xml")
        self.issue_xml = et.parse(issue_file)
        self.xml_root = self.issue_xml.getroot()

    def get_issue_metadata(self):
        """Grabs issue metadata"""

        import datetime
        # pick out items to parse and flatten
        items_to_grab = ['pagerange', 
                'volume', 
                'issue', 
                'string-date', 
                'year','month', 
                'day', 
                'journal-id', 
                'journal-title', 
                'issn']

        #Zip and dict them items with result text returns None if nothing
        self.metadata = self.get_items_by_name(self.xml_root, items_to_grab)

        # get a datetime version for ease later
        self.metadata['pubdate-datetime'] = datetime.date(int(self.metadata['year']), int(self.metadata['month']), int(self.metadata['day']))

    def get_articles(self):
        """ grab articles and associated metadata""" 

        articles = self.xml_root.iter("article")
        article_core = ['identifier', 'url', 'fpage', 'lpage', 'article-title']
        for article in articles:
            article_dict = self.get_items_by_name(article, article_core)
            article_dict['article-type'] = article.get('article-type')
            #append the article dictionary to the article list
            article_dict['contrib-group'] = self.get_contribs(article)
            #attach ocrs and scans
            self.attach_scans_ocrs_to_article(article_dict)
            #Check to see if it's a review
            if article.find('.//product') is not None:
                article_dict['product'] = self.get_product(article) 

            self.articles.append(article_dict)

    def get_product(self, article):
        """grabs product information from an article. 
        
        Returns {'product' String, 'given-names': string, 'surname': string}
        """
        
        items = ['surname', 'given-names', 'source'] 
        
        product = article.find('.//product')
        if product is not None:
            return self.get_items_by_name(product, items)

    def get_contribs(self, article):
        """grabs contribs based on article

        returns [{'given-names':NAME, 'surname': NAME, 'string-name': NAME}]
        """

        contrib_core = ['given-names', 'surname', 'string-name']
        contrib_list = []
        # Grab a list of contributors
 
        if article.find(".//contrib") is not None:
            # Grab the core, sometimes a string-name exists
            for contrib in article.find(".//contrib"):
                contrib_list.append(self.get_items_by_name(contrib, contrib_core))
                #Trap for stand-alone string names which don't always appear with a full
                # list of authors. Note, HD, LI PO
                if contrib.tag  == "string-name":
                    contrib_list.append({'string-name':contrib.text})
        return contrib_list

    def get_scanned_pages(self):
        """
        flatten out the scans, page ids and ocrs. Zip and dict as needed.
        """
        #get page_ids
        self.page_ids = [page.get('id') for page in self.xml_root.iter('page')]
        self.scans = [page.find('pageimage').get('href') for page in self.xml_root.iter('page')]
        self.ocrs = [page.find('ocr').get('href') for page in self.xml_root.iter('page')]
        

    def get_article_pages(self):
        """ create a dictionary out of the article pages. Key is DOI. Simply pair it up.
        Note, there's another method to pair the two together and return a larger 
        dict. This might be redundant. But I left it in case you just wanted DOI/article 
        """
        articleorder = {}
        for article in self.xml_root.iter("seq"):
            doi = article.get("doi").strip("doi-")
            articleorder[doi] = [page.get("rid") for page in article.iter("pageref")]  
             
        self.article_pages= articleorder

    def attach_scans_ocrs_to_article(self, article_dict):
        """takes an article, and matches it to pages, returns list of scans and ocrs
        This is a bit nested, but it helps if you're trying to turn it into json later. 
        One could infer from PID, but explicit makes transforming a one-step-process

        attaches two lists to article based on DOI creating this construct:
        
        Articles = [
        {'identifier': DOI,
        'ocrs': ['href/1.txt', 'href/2.txt' ... etc], 
        'scans': ['href/1.tif', 'href/2.tif' ... etc]} ...ETC]
        """

        #grab the pagelist, create a dict, and find by doi
        ocrs = dict(zip(self.page_ids, self.ocrs)) #zip and dict so we can grab them easier
        scans = dict(zip(self.page_ids, self.scans)) #zip and dict so we can grab them easier
        article_dict['ocrs'] = [ocrs[pid] for pid in self.article_pages[article_dict['identifier']]]
        article_dict['scans'] = [scans[pid] for pid in self.article_pages[article_dict['identifier']]]
    
    def get_article_by_doi(self, doi):
        """return a single article if it matches the DOI"""
        try: 
            return filter(lambda article: article['identifier'] == doi, self.articles)[0]
        except IndexError:
            #return None
            return None

    def attach_group_to_article(self):
        """attach a group to an article if it exists Note, this is a fast and dirty implementation.
        It seems that POETRY always ends with a group section, so this should be OK. If for some
        reason a book ended with a non-named section *after* a named section, this would not work right.
        """
        
        group = self.xml_root.find('./articleorder/group')
        group_title = None # set the group title to none
        
        for item in group.iter():
            if item.tag == "title":
                group_title = item.text
            if item.tag == "article-ref":
                #grab the article by do and attach the group to it
                article = self.get_article_by_doi(item.get('rid').strip('doi-'))

                article['group'] = group_title
    
    def get_json(self, object):
        """serializes an object to json"""
        import json
        return json.dumps(object)

    def json_articles(self):
        """return articles in json"""
        return get_json(self.articles)

    def print_toc(self):
        """
        Print a simple version of the TOC with headers as markdown
        """
        
        #grab the first group
        
        group = self.xml_root.find('./articleorder/group')
        print "# TOC \n"
        for item in group.iter():
            if item.tag == "title":
                print "\n## Section: %s \n" %item.text
            if item.tag == "article-ref":
                doi = item.get('rid').strip('doi-')
                article = self.get_article_by_doi(doi)
                # Some articles (book reviews) have no titles can be odd (1986/vol148_iss2_i20600694)
                
                if article.get('product') is not None:
                    print "**Review of %s by %s %s**" %(article['product'].get('source'), 
                            article['product'].get('given-names'),
                            article['product']['surname'])

                    #Note ,you could also print the author of the book.     
                else: 
                    #print the title
                    print "**%s** " %(article.get('article-title'))
                #Print DOI
                print "  * doi: %s " %doi
                
                #Print authors, check to see if H.D. is in string name. Darn imagist! 
                for author in article.get('contrib-group'):
                    if author.get('string-name') is not None:
                        print "  * %s " %author.get('string-name')
                    if author.get('surname') is not None:
                        print "  * %s %s" %(author.get('given-names'), author.get('surname'))

    def parse(self, issue_path):

        self.get_xml_root(issue_path)
        self.get_issue_metadata()
        self.get_scanned_pages()
        self.get_article_pages()
        self.get_articles()
        self.attach_group_to_article()


