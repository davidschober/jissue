import unittest
from ..jissue import Jissue
import xml.etree.ElementTree as et

#to run tests while developing use python -m jissue.tests.tests 

class TestJissue(unittest.TestCase):

    def setUp(self):
        import os
        self.test_data = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'test_data')
        self.issue = Jissue()
        self.issue.get_xml_root(self.test_data)
        #Basic Test Articles use this for testing articles without parsing everything
        self.articles_mock = [{'identifier': '1', 'article-title':"test1"}, 
                {'identifier': '2', 'article-title':"test2"},
                {'identifier': '3', 'article-title':"test3"},
                {'identifier': '4', 'article-title':"test4"},
                ]

    def test_get_issue_metadata(self):
        self.issue.get_issue_metadata()
        #make sure that the metadata works
        self.assertEqual(self.issue.metadata['volume'], '1')
        self.assertEqual(self.issue.metadata['journal-title'], 'Test Journal')

    def test_get_article_by_doi(self):
        """Verify that I can grab a single article by ID"""
        #add some fake data and test
        self.issue.articles = self.articles_mock
        article = self.issue.get_article_by_doi('1')
        self.assertEqual(article, self.issue.articles[0]) 
    
    def test_attach_group_to_articles(self):
        """make sure that we can attach a group to an article"""

        self.issue.articles = self.articles_mock
        self.issue.attach_group_to_article()
        #verify that the first article has "None" for group
        self.assertEqual(self.issue.articles[0]['group'], None)
        #Make sure an article has a group
        self.assertEqual(self.issue.articles[2]['group'], "test group")

    def test_get_scanned_pages(self):
        """verify scanned pages are grabbed"""
        
        self.issue.get_scanned_pages()
        #Test xml, one article spans two pages
        self.assertEqual(len(self.issue.page_ids), 5)
        self.assertEqual(self.issue.page_ids[0], "p-1")
        self.assertEqual(self.issue.scans[0], "pages/1.tif")

    def test_get_article_pages(self):
        """verify article pages get 4"""
       
        self.issue.get_article_pages()
        self.assertEqual(len(self.issue.article_pages), 4)
     
    def test_get_contribs(self):
        """Verify that contribs are attached to articles"""

        articles = list(self.issue.xml_root.iter('article'))
        #verify that the third article has a contib
        contribs = self.issue.get_contribs(articles[2])
        self.assertEqual(len(contribs), 1)
        #verify that the name is there
        self.assertEqual(contribs[0]['given-names'], 'Test3Given')
    
    def test_get_articles(self):
        """This is really more of an integration test as it uses the main parse function,
        but the article dictionary depends on a lot of other pieces. I might break up the
        pieces to more manageable chuncks if it makes sense later"""

        # Parse the test data
        self.issue.parse(self.test_data)
        self.assertEqual(len(self.issue.articles), 4)
        #validate id
        self.assertEqual(self.issue.articles[0]['identifier'], '1')
        #invalidate ID for 2
        self.assertNotEqual(self.issue.articles[1]['identifier'], '1')
        #validate xml
        self.assertListEqual(self.issue.articles[0]['ocrs'], ['ocrs/1.xml', 'ocrs/2.xml'])
        

if __name__ == '__main__': 
    print "ran it"
    unittest.main()

