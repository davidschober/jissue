from setuptools import setup
# This is the skeleton replace "NAME" with the name of the project
setup(name='jissue',
        version='0.1',
        description='A small library for working with jstor metadata',
        url='https://bitbucket.org/davidschober/jissue',
        author='David Schober',
        author_email='davidschob@gmail.com',
        license='MIT',
        packages=['jissue'],
        #Require packages if you need them
        install_requires=[
            '',
            ],
        #These are used instead of bin packages
        entry_points = {
            #'console_scripts': ['name_of_command=NAME.command_line:main'],
            },
        
        include_package_data=True,
        zip_safe=False)
